// Fill out your copyright notice in the Description page of Project Settings.
#include "BullCowCartridge.h"
#include "HiddenWordsList.h"


void UBullCowCartridge::BeginPlay() // When the game starts
{
    Super::BeginPlay();
	FBullCowCount Count;
	SetupGame();
	PrintLine(TEXT("The Hidden Word is: %s. \n"), *HiddenWord);
}

void UBullCowCartridge::OnInput(const FString& PlayerInput) // When the player hits enter
{

	if (bGameOver)
	{
		ClearScreen();
		SetupGame();
	}
	else
	{
		ProcessGuess(PlayerInput);
	}
}

void UBullCowCartridge::SetupGame()
{

	bGameOver = false;
	Isograms = GetValidWords(WordsToGuess);
	HiddenWord = Isograms[ FMath::RandRange( 0, Isograms.Num() - 1 ) ];
	CharacterCount = HiddenWord.Len();
	Lives = HiddenWord.Len();
	PrintLine(TEXT("HiddenWord: %s"), *HiddenWord);
	PrintLine(TEXT("Hi There!"));
	PrintLine(TEXT("Guess the %i letter word!"), CharacterCount);
	PrintLine(TEXT("You have %i lives to try"), Lives);
	PrintLine(TEXT("Type your guess and press Enter"));
}

void UBullCowCartridge::EndGame()
{
	bGameOver = true;
	PrintLine(TEXT("Game Ended: Press enter to continue"));
}

void UBullCowCartridge::ProcessGuess(const FString& Guess)
{
	PrintLine(Guess);
	if (Guess == HiddenWord)
	{
		PrintLine(TEXT("Right Word! You Win"));
		EndGame();
		return;
	}

	if (Guess.Len() != HiddenWord.Len())
	{
		PrintLine(TEXT("The Hidden word is %i characters long"), CharacterCount);
		PrintLine(TEXT("Sorry, try guessing again. \nYou have %i Lives left"), Lives);
		return;
	}

	if (!IsIsogram(Guess))
	{
		PrintLine(TEXT("No repeating Characters. Guess again!"));
		return;
	}

	--Lives;
	PrintLine(TEXT("You have lost a life"));
	PrintLine(TEXT("%i lifes left"), Lives);

	if (Lives <= 0)
	{
		ClearScreen();
		PrintLine(TEXT("You have no lives left!"));
		PrintLine(TEXT("The Hidden Word was %s"), *HiddenWord);
		EndGame();
		return;
	}

	FBullCowCount Score = GetBullCows(Guess);
	PrintLine(TEXT("You have %i Bulls and %i Cows"), Score.Bulls, Score.Cows);
}

bool UBullCowCartridge::IsIsogram(const FString& Word) const
{
	for (int32 Index = 0; Index < Word.Len(); Index++)
	{
		for (int32 Comparison = Index + 1; Comparison < Word.Len(); Comparison++)
		{
			if (Word[Index] == Word[Comparison])
			{
				return false;
			}
		}

	}
	return true;
}

TArray<FString> UBullCowCartridge::GetValidWords(const TArray<FString>& WordList) const
{
	TArray<FString> ValidWords;

	for (FString CurrentWord : WordList)
	{
		if (CurrentWord.Len() >= 4 && CurrentWord.Len() <= 8 && IsIsogram(CurrentWord))
		{
			ValidWords.Emplace(CurrentWord);
		}
	}

	return ValidWords;
}

FBullCowCount UBullCowCartridge::GetBullCows(const FString& Guess) const
{

	FBullCowCount Count;

	for (int32 GuessIndex = 0; GuessIndex < Guess.Len(); GuessIndex++)
	{
		if (Guess[GuessIndex] == HiddenWord[GuessIndex])
		{
			Count.Bulls++;
			continue;
		}

		for (int32 HiddenIndex = 0; HiddenIndex < HiddenWord.Len(); HiddenIndex++)
		{
			if (Guess[GuessIndex] == HiddenWord[HiddenIndex])
			{
				Count.Cows++;
				break;
			}
		}
	}
	return Count;
}


